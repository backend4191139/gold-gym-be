package goldgym

import (
	"encoding/json"
	"fmt"
	"gold-gym-be/pkg/response"
	"io/ioutil"
	"log"
	"net/http"

	goldEntity "gold-gym-be/internal/entity/goldgym"
	goldStockEntity "gold-gym-be/internal/entity/stock"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"go.uber.org/zap"
)

// Getgoldgym godoc
// @Summary Get entries of all goldgyms
// @Description Get entries of all goldgyms
// @Tags goldgym
// @Accept  json
// @Produce  json
// @Security BearerAuth
// @Success 200
// @Router /v1/profiles [get]
func (h *Handler) InsertGoldGym(w http.ResponseWriter, r *http.Request) {
	var (
		result   interface{}
		metadata interface{}
		err      error

		resp           response.Response
		types          string
		insertgolduser goldEntity.GetGoldUsers
		// insertgoldloginuser      goldEntity.LogUser
		insertgoldsubsuser       goldEntity.InsertSubsAll
		insertgoldsubsuserdetail goldEntity.SubscriptionDetail
		insertstock              goldStockEntity.InsertStockData
	)
	defer resp.RenderJSON(w, r)

	spanCtx, _ := h.tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
	span := h.tracer.StartSpan("Getgoldgym", ext.RPCServerOption(spanCtx))
	defer span.Finish()

	ctx := r.Context()
	ctx = opentracing.ContextWithSpan(ctx, span)
	h.logger.For(ctx).Info("HTTP request received", zap.String("method", r.Method), zap.Stringer("url", r.URL))

	// Your code here
	types = r.FormValue("type")
	switch types {
	case "insertuser":
		body, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal(body, &insertgolduser)
		fmt.Println("Result :", insertgolduser)
		result, err = h.goldgymSvc.InsertGoldUser(ctx, insertgolduser)
		fmt.Println("Result :", insertgolduser)
	// case "loginuser":
	// 	body, _ := ioutil.ReadAll(r.Body)
	// 	json.Unmarshal(body, &insertgoldloginuser)
	// 	fmt.Println("Result :", insertgoldloginuser)
	// 	fmt.Println("Result2 :", &insertgoldloginuser)
	// 	result, metadata, err = h.goldgymSvc.LoginUser(ctx, insertgoldloginuser)
	// 	if err != nil {
	// 		log.Println("err", err)
	// 	}
	// 	fmt.Println("result :", result)
	// 	fmt.Println("metadata :", metadata)
	// 	fmt.Println("err :", err)
	// 	fmt.Println("Result :", insertgoldloginuser)
	case "insertsubsuser":
		body, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal(body, &insertgoldsubsuser)
		fmt.Println("Result :", insertgoldsubsuser)
		fmt.Println("Result2 :", &insertgoldsubsuser)
		result, err = h.goldgymSvc.InsertSubscriptionUser(ctx, insertgoldsubsuser)
		if err != nil {
			log.Println("err", err)
		}
	case "insertsubsuserdetail":
		body, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal(body, &insertgoldsubsuserdetail)
		fmt.Println("Result :", insertgoldsubsuserdetail)
		fmt.Println("Result2 :", &insertgoldsubsuserdetail)
		result, err, resp = h.goldgymSvc.InsertSubscriptionDetail(ctx, insertgoldsubsuserdetail)
		if err != nil {
			log.Println("err", err)
		}
	case "insertstock":
		body, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal(body, &insertstock)
		fmt.Println("Result :", insertstock)
		fmt.Println("Result2 :", &insertstock)
		result, err = h.goldgymSvcStock.InsertStockSales(ctx, insertstock)
		if err != nil {
			log.Println("err", err)
		}
		// case "":
	}

	if err != nil {
		resp.SetError(err, http.StatusInternalServerError)
		resp.StatusCode = 500
		log.Printf("[ERROR] %s %s - %s\n", r.Method, r.URL, err.Error())
		return
	}

	resp.Data = result
	resp.Metadata = metadata
	log.Printf("[INFO] %s %s\n", r.Method, r.URL)
	h.logger.For(ctx).Info("HTTP request done", zap.String("method", r.Method), zap.Stringer("url", r.URL))

	return
}
